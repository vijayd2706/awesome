package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class HomePage extends ProjectMethods{

	public HomePage()
	{
		PageFactory.initElements(driver, this);
	}
	
	//@FindBy()
	
	public LoginPage clickLogOut() {
		WebElement eleLogOut = locateElement("class", "decorativeSubmit");
	    click(eleLogOut);  
	    return new LoginPage();
	}
	@FindBy(linkText="CRM/SFA")
	WebElement clickLinkCrm;
	    public MyHomePage clickCRM()
	    {
	    	click(clickLinkCrm);
	    return	new MyHomePage();
	    
	    }
	    
	    
}
	
	








