package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class ViewLead extends ProjectMethods{

	public ViewLead()
	{
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(xpath="//a[text()='Find Leads']")
	WebElement findLeadButton;
 public FindLeadPage findLead() {
	 click(findLeadButton);
	 return new FindLeadPage();
 }
 
 

}
	



