package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLead extends ProjectMethods {

	public CreateLead()
	{
		PageFactory.initElements(driver, this);
	}

	@FindBy(id="createLeadForm_companyName")
	WebElement companyName;
	public CreateLead enterCompanyName(String data) {
		type(companyName, data);
		return this;
	}

	@FindBy(id="createLeadForm_firstName")
	WebElement firstName;
	public CreateLead enterfirstName(String data) {
		type(firstName, data);
		return this;
	}


	@FindBy(id="createLeadForm_lastName")
	WebElement lastName;
	public CreateLead enterlastName(String data) {
		type(lastName, data);
		return this;
	}


	@FindBy(className="smallSubmit")
	WebElement submitButton;
	public ViewLead clickCreateLead() 
	{

		click(submitButton);
		return	new ViewLead();


	}


}







