package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyLead extends ProjectMethods{

	public MyLead()
	{
		PageFactory.initElements(driver, this);
	}
	
	
	
	@FindBy(linkText="Create Lead")
	WebElement MyLead;
	    public CreateLead clickMyLead()
	    {
	    	click(MyLead);
	    	return new CreateLead();
	    	
	    }
	    @FindBy(linkText="Merge Leads")
	    WebElement clickMergeLead;
	    
	    public MergeLeadForm clickMergeLead()
	    {
	    	click(clickMergeLead);
	    	return new MergeLeadForm();
	    }
}
	








