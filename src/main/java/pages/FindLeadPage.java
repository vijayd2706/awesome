package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import wdMethods.ProjectMethods;

public class FindLeadPage extends ProjectMethods{
	
	public String ValidationText;

	public FindLeadPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(xpath="//input[@name='id']")
	WebElement findLeadButton;
 public FindLeadPage findLeadInput() {
	 String leadID = null;
	type(findLeadButton,leadID);
	 return this;
 }
  @FindBy(xpath="//button[text()='Find Leads']")
  WebElement findLeadsButton;
 public FindLeadPage findLeads()  {
	
	click(findLeadsButton);
	try {
		Thread.sleep(2000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} 
	return this;
 }
 
 @FindBy(xpath="//div[@class='x-paging-info']")
 WebElement text;
 
 public FindLeadPage validateText(String data) {
	 verifyExactText(text, data);
	return this; 
	 
 }

}
	



