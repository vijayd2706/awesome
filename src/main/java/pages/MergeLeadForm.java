package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MergeLeadForm extends ProjectMethods{

	public MergeLeadForm()
	{
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(xpath="(//div[@class='subSectionBlock']//img)[1]")
	WebElement clickFromLead;
	public MergeLeadForm fromLead() {
		click(clickFromLead);
		switchToWindow(1);
		return this;
		
	}
	 @FindBy(xpath="(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]//a")
	  WebElement img1;
	public MergeLeadForm getText(String data) {
		String leadID = img1.getText();
		return this;
		}
	
	 @FindBy(xpath="(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]//a")
	  WebElement eleimg1;
	public MergeLeadForm lookUpLeads() {
		click(eleimg1);
		switchToWindow(0);
		return this;
		}
	
	 @FindBy(xpath="(//div[@class='subSectionBlock']//img)[2]")
	  WebElement img2;
	public MergeLeadForm formLead1() {
		click(img2);
		switchToWindow(1);
		return this;
		}
	

	 @FindBy(xpath="(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[2]//a")
	  WebElement eleimg2;
	public MergeLeadForm lookUpLeads1() {
		click(eleimg2);
		switchToWindow(0);
		return this;
		}
	
	@FindBy(xpath="//a[@class='buttonDangerous']")
	WebElement button;
	public MergeLeadForm mergeButton() {
		click(button);
		return this;
		
	}
	
	public ViewLead handleAlert() {
		acceptAlert();
		return new ViewLead();
	}
	


}



	









