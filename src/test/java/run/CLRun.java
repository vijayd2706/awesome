package run;



	
	import org.junit.runner.RunWith;

	import cucumber.api.CucumberOptions;
	import cucumber.api.SnippetType;
	import cucumber.api.junit.Cucumber;

	@RunWith(Cucumber.class)
	@CucumberOptions(
			features= {"src/test/java/feature/CL.feature"},
	glue="steps",
	dryRun=true,
	snippets=SnippetType.CAMELCASE,
	monochrome=true,
	plugin= {"pretty","html:CucumberREports"}
	//name="Login"
	/*tags= {"@smoke"}*/)
	public class CLRun {

	}


