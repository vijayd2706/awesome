package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class Tips {
	
	public ChromeDriver driver;
	@Given("Launch Browser")
	public void LaunchBrowser() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/");
	}
   /*@And("Load URL")
	public void LoadURl() {
	   driver.get("http://leaftaps.com/opentaps/");
	}*/

	@And("Maximize")
	public void maximize() {
		driver.manage().window().maximize();
	}

	@And("Set timeout")
	public void setTimeout() {
	  driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@And("Enter UName (.*)")
	public void enterUName(String string) {
		driver.findElementById("username").sendKeys("DemoSalesManager");
	}

	@And("Enter Password (.*)")
	public void enterPassword(String string) {
		driver.findElementById("password").sendKeys("crmsfa");  
	}

	@When("Click Login")
	public void clickLogin() {
		driver.findElementByClassName("decorativeSubmit").click();
	}

	@Then("Verify Welcome (.*)")
	public void verifyWelcome(String verifyName) {
		String text = driver.findElementByTagName("h2").getText();
		System.out.println(text);
		if(text.contains(verifyName)) {
			System.out.println("pass");
		} else {
			System.out.println("fail");
		}
		driver.close();
	}



}
